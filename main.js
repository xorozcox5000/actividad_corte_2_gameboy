const playBtn =document.querySelector(".Imagen_Referencia .boton_1 .controls .playBtn");
const VolumenUP=document.querySelector(".Imagen_Referencia .cruzUp");
const VolumenDown=document.querySelector(".Imagen_Referencia .cruzDown");
const Skip_D=document.querySelector(".Imagen_Referencia .cruzRigth");
const Skip_L=document.querySelector(".Imagen_Referencia .cruzLeft");
const Skip_time_D=document.querySelector(".Imagen_Referencia .boton_select");
const Skip_time_L=document.querySelector(".Imagen_Referencia .boton_start");
const cancion=["../Canciones/The legends never die.mp3","../Canciones/League of Legends.mp3","../Canciones/THE PHOENIX.mp3"];

const controls={
    play: playBtn,
    volumenUP: VolumenUP,
    volumenDown: VolumenDown,
    Skip_D: Skip_D,
    Skip_L: Skip_L,
    Skip_time_D: Skip_time_D,
    Skip_time_L: Skip_time_L

}


const player = new AudioPlayer(cancion,document.querySelector(".Pantalla"),controls);
