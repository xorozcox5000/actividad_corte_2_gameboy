var i=0;
class AudioPlayer{
    constructor(src,domElement,controls){
        this.audio= new Audio();
        this.Cambiar_Cancion(src,controls);       
        this.controls=controls; 
        this.domElement=domElement;
        this.progress=this.domElement.querySelector(".Pantalla .progress")
        this.initControls();
        
        
    }

    initControls(){
        if(this.controls.play){
            this.initPlay(this.controls.play);
        }
        this.volumen(controls);
        this.Skip_time(controls);
    }

    initPlay(domElement){
        domElement.onclick= () => {
            const icon = domElement.querySelector("i");
            const isPaused = icon.classList.contains("fa-play");
            if(!isPaused){
                icon.classList.replace("fa-pause","fa-play");
                this.pause();
            }else{
                icon.classList.replace("fa-play","fa-pause");
                this.play();
            }
        }
    }

    updateUI(){
        
        const total=this.audio.duration;
        const current = this.audio.currentTime;
        const progress= (current/total)*100;
        this.progress.style.width= `${progress}%`;

    }

    play(){
        this.audio.play().then().catch(err => console.log(`No tienes ninguna cancion puesta we `));
    }

    pause(){
        this.audio.pause();
    }

    volumen(controls){
        controls.volumenUP.onclick= () => {
            if(this.audio.volume<1){
                this.audio.volume+=0.1;
            }
                     
        }
        controls.volumenDown.onclick= () =>{
            if(this.audio.volume>0.1){
                this.audio.volume-=0.1;
            }
            
        }
    }


    Skip_time(controls){
        controls.Skip_time_D.onclick= ()=>{
            this.audio.currentTime+=10;
        }

        controls.Skip_time_L.onclick= () =>{
            this.audio.currentTime-=10;
        }

    }

    Cambiar_Cancion(src,controls){

        controls.Skip_D.onclick= () =>{
            if(!this.audio.pause()){
                if(i>2){
                    i=0;
                    
                }
                this.cancion=src[i++];
                this.audio= new Audio(this.cancion);
                this.audio.play().then().catch(err => console.log(`calma we dale otra vez <3`));
                this.audio.ontimeupdate= () => {this.updateUI();}
                console.log("derecha: ",i);
                
            }
        }
        controls.Skip_L.onclick= () =>{
            
    
            if(!this.audio.pause()){
                if(i<0){
                    i=2;
                }
                this.cancion=src[i--];
                this.audio= new Audio(this.cancion);
                this.audio.play().then().catch(err => console.log(`calma we dale otra vez <3`));
                this.audio.ontimeupdate= () => {this.updateUI();}
                console.log("izq: ",i);
                
            }
        }
    }
}